! ---------------------------------------------------------------
!  COND_SPLIT  This routine solves the heat flux following the  
!              anistropic heat conduction.
!
!  inputs/outputs
!  uin         => (const)  input state
!  gravin      => (const)  input gravitational acceleration
!  iu1,iu2     => (const)  first and last index of input array,
!  ju1,ju2     => (const)  cell centered,    
!  ku1,ku2     => (const)  including buffer cells.
!  flux       <=  (modify) return fluxes in the 3 coord directions
!  if1,if2     => (const)  first and last index of output array,
!  jf1,jf2     => (const)  edge centered,
!  kf1,kf2     => (const)  for active cells only.
!  dx,dy,dz    => (const)  (dx,dy,dz)
!  dt          => (const)  time step
!  ngrid       => (const)  number of sub-grids
!  ndim        => (const)  number of dimensions
!
!  uin = (\rho, \rho u, \rho v, \rho w, Etot, A, B, C)
!  the hydro variable are cell-centered
!  whereas the magnetic field B=(A,B,C) are face-centered.
!  Note that here we have 3 components for v and B whatever ndim.
!
!  This routine was written by Yohan Dubois & Benoit Commerçon
! ----------------------------------------------------------------
subroutine cond_split(uin,flux,dx,dy,dz,dt,ngrid,compute,fdx)
  use amr_parameters
  use const             
  use hydro_parameters
  use pm_commons, ONLY: localseed,iseed
  use amr_commons, ONLY:ncpu,myid
  use cooling_module,ONLY:kB,mH,clight
!  use radiation_parameters
  implicit none 

  integer ::ngrid,compute
  real(dp)::dx,dy,dz,dt

  ! Input states
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3)::uin
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::fdx

  ! Output fluxes
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2,1:nvar,1:ndim)::flux
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2)::Xflux,Yflux,Zflux

  ! Primitive variables
  real(dp),dimension(1:nvector,iu1:iu2+1,ju1:ju2+1,ku1:ku2+1,1:3),save::bf  
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2),save::kspitzer
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2),save::Temp,Temp2,dens
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2),save::ffdx

  real(dp)::Temp_K,bx,by,bz,kpar,dTdx,dTdy,dTdz,fx

  ! Local scalar variables
  integer::i,j,k,l,ivar
  integer::ilo,ihi,jlo,jhi,klo,khi

  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::mu,scale_TK,scale_kspitzer,kappa_coef
  real(dp)::eps,Tp_loc,rho,scale_kappa,kpara_ana

  real(dp)::n_e,flux_sat,scale_jsat
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2),save::jsat
  integer ::ind_Teold

  ind_Teold = 3 ! Stored in a different index than in other routines (nvar+3)

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
!!$  mu=0.5882353
!!$  scale_TK=scale_T2*mu
  scale_kspitzer=scale_d*scale_l**4d0/scale_t**3/scale_T2
  scale_jsat=scale_d*scale_v**2d0/scale_t
  scale_kappa=scale_d*scale_l*scale_v**3
  
  ilo=MIN(1,iu1+2); ihi=MAX(1,iu2-2)
  jlo=MIN(1,ju1+2); jhi=MAX(1,ju2-2)
  klo=MIN(1,ku1+2); khi=MAX(1,ku2-2)

 ! kappa_coef=1.84d-5/coulomb_log*scale_TK**2.5d0/scale_kspitzer * f_spitzer

  do k = ku1, ku2
  do j = ju1, ju2
  do i = iu1, iu2
     do l = 1, ngrid

        Tp_loc = uin(l,i,j,k,ind_Teold)

        dens(l,i,j,k)=uin(l,i,j,k,1)*scale_d
        
        !oneoverkspitzer(l,i,j,k)=scale_kappa/kpara_ana(Tp_loc)       !kappa_coef*(Tp_loc**2.5)
        kspitzer(l,i,j,k)=uin(l,i,j,k,4)
        bf(l,i,j,k,1)=uin(l,i,j,k,6)
        bf(l,i,j,k,2)=uin(l,i,j,k,7)
        bf(l,i,j,k,3)=uin(l,i,j,k,8)
        
        ffdx(l,i,j,k)=fdx(l,i,j,k)

        if(compute==1)Temp(l,i,j,k)=Tp_loc
        if(compute==2)Temp(l,i,j,k)=uin(l,i,j,k,2)

        Temp2(l,i,j,k)=Tp_loc
     end do
  enddo
  enddo
  enddo

 ! Compute the heat flux in X direction
  call cmpXheatflx(Temp,dens,bf,kspitzer,Xflux,dx,dy,dz,dt,ngrid,compute,ffdx,Temp2)
  do k=klo,khi
  do j=jlo,jhi
  do i=if1,if2
     do l = 1, ngrid
        flux(l,i,j,k,5,1)=Xflux(l,i,j,k)
    enddo
  enddo
  enddo
  enddo

#if NDIM>1
  ! Compute the heat flux in Y direction
  call cmpYheatflx(Temp,dens,bf,kspitzer,Yflux,dx,dy,dz,dt,ngrid,compute,ffdx,Temp2)
  do k=klo,khi
  do j=jf1,jf2
  do i=ilo,ihi
     do l = 1, ngrid
        flux(l,i,j,k,5,2)=Yflux(l,i,j,k)
     enddo
  enddo
  enddo
  enddo
#endif
#if NDIM>2
  ! Compute the heat flux in Z direction
  call cmpZheatflx(Temp,dens,bf,kspitzer,Zflux,dx,dy,dz,dt,ngrid,compute,ffdx,Temp2)
  do k=kf1,kf2
  do j=jlo,jhi
  do i=ilo,ihi
     do l = 1, ngrid
        flux(l,i,j,k,5,3)=Zflux(l,i,j,k)
     enddo
  enddo
  enddo
  enddo
#endif
end subroutine cond_split
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine cmpXheatflx(Temp,dens,bf,kspitzer,myflux,dx,dy,dz,dt,ngrid,compute,ffdx,Temp2)
  use amr_parameters
  use const             
  use hydro_parameters
  implicit none 

  integer ::ngrid,compute
  real(dp)::dx,dy,dz,dt

  ! Output fluxes
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2)::myflux

  ! Primitive variables
  !real(dp),dimension(1:nvector,iu1:iu2+1,ju1:ju2+1,ku1:ku2+1,1:3)::xloc
  real(dp),dimension(1:nvector,iu1:iu2+1,ju1:ju2+1,ku1:ku2+1,1:3)::bf  
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::kspitzer
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::Temp,Temp2,dens
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::ffdx

  real(dp)::Bnorm,fx,oneovertwodx,oneoverfourdx
  real(dp)::dTdx1,dTdx2,dTdx3,dTdx4,famr
  real(dp)::dTdy1,dTdy2,dTdy3,dTdy4
  real(dp)::dTdz1,dTdz2,dTdz3,dTdz4
  real(dp)::bx1,bx2,bx3,bx4
  real(dp)::by1,by2,by3,by4
  real(dp)::bz1,bz2,bz3,bz4
  real(dp)::fx1,fx2,fx3,fx4
  real(dp)::kparx1,kparx2,kparx3,kparx4,oneminuskperp,dmean,tmean,maxflux

  ! Local scalar variables
  integer::i,j,k,l,ivar
  integer::jlo,jhi,klo,khi

  jlo=MIN(1,ju1+2); jhi=MAX(1,ju2-2)
  klo=MIN(1,ku1+2); khi=MAX(1,ku2-2)

  oneovertwodx =0.50d0/dx
  oneoverfourdx=0.25d0/dx
  oneminuskperp=1.0d0-k_perp

if(isotrope_cond)then
  do k=klo,khi
  do j=jlo,jhi
  do i=if1,if2
     do l = 1, ngrid
        dTdx1  =(Temp(l,i,j,k)-Temp(l,i-1,j,k))/dx
        kparx1=0.5d0*(kspitzer(l,i-1,j,k)+kspitzer(l,i,j,k))
        fx    =kparx1*dTdx1
        famr=max(ffdx(l,i,j,k),ffdx(l,i-1,j,k))
        fx=fx/famr
        myflux(l,i,j,k)=fx*dt/dx
        if(compute==3)myflux(l,i,j,k)=kparx1*dt/dx**2/famr
     enddo
  enddo
  enddo
  enddo

else

  if (.not.slopelim_cond)then
  do k=klo,khi
  do j=jlo,jhi
  do i=if1,if2
     do l = 1, ngrid
#if NDIM==1
!!$        1-------

!!$        dTdx1=(Temp(l,i,j,k)-Temp(l,i-1,j,k))/dx
!!$        !kparx1=2d0/(oneoverkspitzer(l,i-1,j,k)+oneoverkspitzer(l,i,j,k)) ! harmonic mean
!!$        kparx1=0.5d0*(kspitzer(l,i-1,j,k)+kspitzer(l,i-1,j,k)) ! arithmetic mean
!!$        fx=kparx1*dTdx1
!!$        if(compute==3)fx=kparx1/dx
!!$        famr=max(ffdx(l,i,j,k),ffdx(l,i-1,j,k))
!!$        fx=fx/famr

        if(compute .ne. 3) dTdx1=(Temp(l,i,j,k)-Temp(l,i-1,j,k))/dx
        !kparx1=2d0/(oneoverkspitzer(l,i-1,j,k)+oneoverkspitzer(l,i,j,k)) ! harmonic mean
        kparx1=0.5d0*(kspitzer(l,i-1,j,k)+kspitzer(l,i-1,j,k)) ! arithmetic mean
        if(compute .ne. 3)then
           fx=kparx1*dTdx1
        else 
           fx=kparx1/dx
        endif
        famr=max(ffdx(l,i,j,k),ffdx(l,i-1,j,k))
        fx=fx/famr

#endif
#if NDIM==2
!!$        2-------
!!$        |      |
!!$        |      |
!!$        |      |
!!$        1-------
        
        if(compute .ne. 3)then   
           dTdx1=(Temp(l,i  ,j  ,k)+Temp(l,i  ,j-1,k) &
                - Temp(l,i-1,j  ,k)-Temp(l,i-1,j-1,k))*oneovertwodx
           dTdx2=(Temp(l,i  ,j+1,k)+Temp(l,i  ,j  ,k) &
                - Temp(l,i-1,j+1,k)-Temp(l,i-1,j  ,k))*oneovertwodx        
           
           dTdy1=(Temp(l,i  ,j  ,k)+Temp(l,i-1,j  ,k) &
                - Temp(l,i  ,j-1,k)-Temp(l,i-1,j-1,k))*oneovertwodx
           dTdy2=(Temp(l,i  ,j+1,k)+Temp(l,i-1,j+1,k) &
                - Temp(l,i  ,j  ,k)-Temp(l,i-1,j  ,k))*oneovertwodx
        endif
        
        bx1=0.5d0*(bf(l,i  ,j-1,k,1)+bf(l,i  ,j  ,k,1))
        bx2=0.5d0*(bf(l,i  ,j  ,k,1)+bf(l,i  ,j+1,k,1))
        
        by1=0.5d0*(bf(l,i-1,j  ,k,2)+bf(l,i  ,j  ,k,2))
        by2=0.5d0*(bf(l,i-1,j+1,k,2)+bf(l,i  ,j+1,k,2))
        
        Bnorm=sqrt(bx1*bx1+by1*by1)
        if(Bnorm.gt.0.0)then
           bx1=bx1/Bnorm
           by1=by1/Bnorm
        endif
        Bnorm=sqrt(bx2*bx2+by2*by2)
        if(Bnorm.gt.0.0)then
           bx2=bx2/Bnorm
           by2=by2/Bnorm
        endif

        kparx1=0.25d0*(kspitzer(l,i  ,j  ,k)+kspitzer(l,i  ,j-1,k) &
             +      kspitzer(l,i-1,j  ,k)+kspitzer(l,i-1,j-1,k))
        kparx2=0.25d0*(kspitzer(l,i  ,j+1,k)+kspitzer(l,i  ,j  ,k) &
             +      kspitzer(l,i-1,j+1,k)+kspitzer(l,i-1,j  ,k))

        if(compute .ne. 3)then   
           fx1=kparx1*(bx1*oneminuskperp*(bx1*dTdx1+by1*dTdy1)+k_perp*dTdx1)
           fx2=kparx2*(bx2*oneminuskperp*(bx2*dTdx2+by2*dTdy2)+k_perp*dTdx2)
        else ! Preconditionner
           fx1=kparx1*(bx1*oneminuskperp*(bx1+by1)+k_perp)
           fx2=kparx2*(bx2*oneminuskperp*(bx2+by2)+k_perp)
        end if
        fx=0.5d0*(fx1+fx2)
#endif
#if NDIM==3
!!$          4--------
!!$         / |      /|
!!$        /  |     / |
!!$        3-------   |
!!$        |  |   |   |
!!$        | /2   |  /
!!$        |/     | /
!!$        1-------
        
        ! Centered symmetric scheme
        if(compute .ne. 3)then   
           dTdx1=(Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  )+Temp(l,i  ,j  ,k-1)+Temp(l,i  ,j-1,k-1) &
                - Temp(l,i-1,j  ,k  )-Temp(l,i-1,j-1,k  )-Temp(l,i-1,j  ,k-1)-Temp(l,i-1,j-1,k-1))*oneoverfourdx
           dTdx2=(Temp(l,i  ,j+1,k  )+Temp(l,i  ,j  ,k  )+Temp(l,i  ,j+1,k-1)+Temp(l,i  ,j  ,k-1) &
                - Temp(l,i-1,j+1,k  )-Temp(l,i-1,j  ,k  )-Temp(l,i-1,j+1,k-1)-Temp(l,i-1,j  ,k-1))*oneoverfourdx
           dTdx3=(Temp(l,i  ,j  ,k+1)+Temp(l,i  ,j-1,k+1)+Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  ) &
                - Temp(l,i-1,j  ,k+1)-Temp(l,i-1,j-1,k+1)-Temp(l,i-1,j  ,k  )-Temp(l,i-1,j-1,k  ))*oneoverfourdx
           dTdx4=(Temp(l,i  ,j+1,k+1)+Temp(l,i  ,j  ,k+1)+Temp(l,i  ,j+1,k  )+Temp(l,i  ,j  ,k  ) &
                - Temp(l,i-1,j+1,k+1)-Temp(l,i-1,j  ,k+1)-Temp(l,i-1,j+1,k  )-Temp(l,i-1,j  ,k  ))*oneoverfourdx

           dTdy1=(Temp(l,i  ,j  ,k  )+Temp(l,i-1,j  ,k  )+Temp(l,i  ,j  ,k-1)+Temp(l,i-1,j  ,k-1) &
                - Temp(l,i  ,j-1,k  )-Temp(l,i-1,j-1,k  )-Temp(l,i  ,j-1,k-1)-Temp(l,i-1,j-1,k-1))*oneoverfourdx
           dTdy2=(Temp(l,i  ,j+1,k  )+Temp(l,i-1,j+1,k  )+Temp(l,i  ,j+1,k-1)+Temp(l,i-1,j+1,k-1) &
                - Temp(l,i  ,j  ,k  )-Temp(l,i-1,j  ,k  )-Temp(l,i  ,j  ,k-1)-Temp(l,i-1,j  ,k-1))*oneoverfourdx
           dTdy3=(Temp(l,i  ,j  ,k+1)+Temp(l,i-1,j  ,k+1)+Temp(l,i  ,j  ,k  )+Temp(l,i-1,j  ,k  ) &
                - Temp(l,i  ,j-1,k+1)-Temp(l,i-1,j-1,k+1)-Temp(l,i  ,j-1,k  )-Temp(l,i-1,j-1,k  ))*oneoverfourdx
           dTdy4=(Temp(l,i  ,j+1,k+1)+Temp(l,i-1,j+1,k+1)+Temp(l,i  ,j+1,k  )+Temp(l,i-1,j+1,k  ) &
                - Temp(l,i  ,j  ,k+1)-Temp(l,i-1,j  ,k+1)-Temp(l,i  ,j  ,k  )-Temp(l,i-1,j  ,k  ))*oneoverfourdx
           
           dTdz1=(Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  )+Temp(l,i-1,j  ,k  )+Temp(l,i-1,j-1,k  ) &
                - Temp(l,i  ,j  ,k-1)-Temp(l,i  ,j-1,k-1)-Temp(l,i-1,j  ,k-1)-Temp(l,i-1,j-1,k-1))*oneoverfourdx
           dTdz2=(Temp(l,i  ,j+1,k  )+Temp(l,i  ,j  ,k  )+Temp(l,i-1,j+1,k  )+Temp(l,i-1,j  ,k  ) &
                - Temp(l,i  ,j+1,k-1)-Temp(l,i  ,j  ,k-1)-Temp(l,i-1,j+1,k-1)-Temp(l,i-1,j  ,k-1))*oneoverfourdx
           dTdz3=(Temp(l,i  ,j  ,k+1)+Temp(l,i-1,j  ,k+1)+Temp(l,i  ,j-1,k+1)+Temp(l,i-1,j-1,k+1) &
                - Temp(l,i  ,j  ,k  )-Temp(l,i-1,j  ,k  )-Temp(l,i  ,j-1,k  )-Temp(l,i-1,j-1,k  ))*oneoverfourdx
           dTdz4=(Temp(l,i  ,j+1,k+1)+Temp(l,i-1,j+1,k+1)+Temp(l,i  ,j  ,k+1)+Temp(l,i-1,j  ,k+1) &
                - Temp(l,i  ,j+1,k  )-Temp(l,i-1,j+1,k  )-Temp(l,i  ,j  ,k  )-Temp(l,i-1,j  ,k  ))*oneoverfourdx
        end if

        bx1=0.25d0*(bf(l,i  ,j-1,k-1,1)+bf(l,i  ,j  ,k-1,1)+bf(l,i  ,j-1,k  ,1)+bf(l,i  ,j  ,k  ,1))
        bx2=0.25d0*(bf(l,i  ,j  ,k-1,1)+bf(l,i  ,j+1,k-1,1)+bf(l,i  ,j  ,k  ,1)+bf(l,i  ,j+1,k  ,1))
        bx3=0.25d0*(bf(l,i  ,j-1,k  ,1)+bf(l,i  ,j  ,k  ,1)+bf(l,i  ,j-1,k+1,1)+bf(l,i  ,j  ,k+1,1))
        bx4=0.25d0*(bf(l,i  ,j  ,k  ,1)+bf(l,i  ,j+1,k  ,1)+bf(l,i  ,j  ,k+1,1)+bf(l,i  ,j+1,k+1,1))

        by1=0.25d0*(bf(l,i-1,j  ,k-1,2)+bf(l,i  ,j  ,k-1,2)+bf(l,i-1,j  ,k  ,2)+bf(l,i  ,j  ,k  ,2))
        by2=0.25d0*(bf(l,i  ,j+1,k-1,2)+bf(l,i-1,j+1,k-1,2)+bf(l,i  ,j+1,k  ,2)+bf(l,i-1,j+1,k  ,2))
        by3=0.25d0*(bf(l,i-1,j  ,k  ,2)+bf(l,i  ,j  ,k  ,2)+bf(l,i-1,j  ,k+1,2)+bf(l,i  ,j  ,k+1,2))
        by4=0.25d0*(bf(l,i  ,j+1,k  ,2)+bf(l,i-1,j+1,k  ,2)+bf(l,i  ,j+1,k+1,2)+bf(l,i-1,j+1,k+1,2))
        
        bz1=0.25d0*(bf(l,i-1,j-1,k  ,3)+bf(l,i-1,j  ,k  ,3)+bf(l,i  ,j-1,k  ,3)+bf(l,i  ,j  ,k  ,3))
        bz2=0.25d0*(bf(l,i-1,j  ,k  ,3)+bf(l,i-1,j+1,k  ,3)+bf(l,i  ,j  ,k  ,3)+bf(l,i  ,j+1,k  ,3))
        bz3=0.25d0*(bf(l,i  ,j-1,k+1,3)+bf(l,i  ,j  ,k+1,3)+bf(l,i-1,j-1,k+1,3)+bf(l,i-1,j  ,k+1,3))
        bz4=0.25d0*(bf(l,i  ,j  ,k+1,3)+bf(l,i  ,j+1,k+1,3)+bf(l,i-1,j  ,k+1,3)+bf(l,i-1,j+1,k+1,3))

        Bnorm=sqrt(bx1*bx1+by1*by1+bz1*bz1)
        if(Bnorm.gt.0.0)then
           bx1=bx1/Bnorm
           by1=by1/Bnorm
           bz1=bz1/Bnorm
        endif
        Bnorm=sqrt(bx2*bx2+by2*by2+bz2*bz2)
        if(Bnorm.gt.0.0)then
           bx2=bx2/Bnorm
           by2=by2/Bnorm
           bz2=bz2/Bnorm
        endif
        Bnorm=sqrt(bx3*bx3+by3*by3+bz3*bz3)
        if(Bnorm.gt.0.0)then
           bx3=bx3/Bnorm
           by3=by3/Bnorm
           bz3=bz3/Bnorm
        endif
        Bnorm=sqrt(bx4*bx4+by4*by4+bz4*bz4)
        if(Bnorm.gt.0.0)then
           bx4=bx4/Bnorm
           by4=by4/Bnorm
           bz4=bz4/Bnorm
        endif

        kparx1=0.125d0*(kspitzer(l,i  ,j  ,k  )+kspitzer(l,i  ,j-1,k  )+kspitzer(l,i  ,j  ,k-1) &
             +      kspitzer(l,i  ,j-1,k-1)+kspitzer(l,i-1,j  ,k  )+kspitzer(l,i-1,j-1,k  ) &
             +      kspitzer(l,i-1,j  ,k-1)+kspitzer(l,i-1,j-1,k-1))
        kparx2=0.125d0*(kspitzer(l,i  ,j+1,k  )+kspitzer(l,i  ,j  ,k  )+kspitzer(l,i  ,j+1,k-1) &
             +      kspitzer(l,i  ,j  ,k-1)+kspitzer(l,i-1,j+1,k  )+kspitzer(l,i-1,j  ,k  ) &
             +      kspitzer(l,i-1,j+1,k-1)+kspitzer(l,i-1,j  ,k-1))
        kparx3=0.125d0*(kspitzer(l,i  ,j  ,k+1)+kspitzer(l,i  ,j-1,k+1)+kspitzer(l,i  ,j  ,k  ) &
             +      kspitzer(l,i  ,j-1,k  )+kspitzer(l,i-1,j  ,k+1)+kspitzer(l,i-1,j-1,k+1) &
             +      kspitzer(l,i-1,j  ,k  )+kspitzer(l,i-1,j-1,k  ))
        kparx4=0.125d0*(kspitzer(l,i  ,j+1,k+1)+kspitzer(l,i  ,j  ,k+1)+kspitzer(l,i  ,j+1,k  ) &
             +      kspitzer(l,i  ,j  ,k  )+kspitzer(l,i-1,j+1,k+1)+kspitzer(l,i-1,j  ,k+1) &
             +      kspitzer(l,i-1,j+1,k  )+kspitzer(l,i-1,j  ,k  ))

        if(compute .ne. 3)then   
           fx1=kparx1*(bx1*oneminuskperp*(bx1*dTdx1+by1*dTdy1+bz1*dTdz1)+k_perp*dTdx1)
           fx2=kparx2*(bx2*oneminuskperp*(bx2*dTdx2+by2*dTdy2+bz2*dTdz2)+k_perp*dTdx2)
           fx3=kparx3*(bx3*oneminuskperp*(bx3*dTdx3+by3*dTdy3+bz3*dTdz3)+k_perp*dTdx3)
           fx4=kparx4*(bx4*oneminuskperp*(bx4*dTdx4+by4*dTdy4+bz4*dTdz4)+k_perp*dTdx4)
        else ! Preconditionner
           fx1=kparx1*(bx1*oneminuskperp*(bx1+by1+bz1)+k_perp)
           fx2=kparx2*(bx2*oneminuskperp*(bx2+by2+bz2)+k_perp)
           fx3=kparx3*(bx3*oneminuskperp*(bx3+by3+bz3)+k_perp)
           fx4=kparx4*(bx4*oneminuskperp*(bx4+by4+bz4)+k_perp)
        end if
        fx=0.25d0*(fx1+fx2+fx3+fx4)
#endif
        if(saturation)then
           dmean = 0.5d0*(dens (l,i-1,j,k)+dens (l,i,j,k))
           tmean = 0.5d0*(Temp2(l,i-1,j,k)+Temp2(l,i,j,k))
           if(compute .ne. 3)then
              maxflux=fudge_sat*tmean**1.5*dmean
           else
              maxflux=fudge_sat*tmean**1.5*dmean/(Temp2(l,i,j,k)-Temp2(l,i-1,j,k))*dx*famr
           endif
           if(fx.lt.0.0d0)fx=MAX(fx,-maxflux)
           if(fx.gt.0.0d0)fx=MIN(fx, maxflux)
        endif
        myflux(l,i,j,k)=fx*dt/dx
     enddo
  enddo
  enddo
  enddo
endif

  if (slopelim_cond)then
     ! TODO
  endif
endif

end subroutine cmpXheatflx
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine cmpYheatflx(Temp,dens,bf,kspitzer,myflux,dx,dy,dz,dt,ngrid,compute,ffdx,Temp2)
  use amr_parameters
  use const             
  use hydro_parameters
  implicit none 

  integer ::ngrid,compute
  real(dp)::dx,dy,dz,dt

  ! Output fluxes
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2)::myflux

  ! Primitive variables
  real(dp),dimension(1:nvector,iu1:iu2+1,ju1:ju2+1,ku1:ku2+1,1:3)::bf  
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::kspitzer
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::Temp,Temp2,dens
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::ffdx

  real(dp)::Bnorm,fy,oneovertwodx,oneoverfourdx,famr
  real(dp)::dTdx1,dTdx2,dTdx3,dTdx4
  real(dp)::dTdy1,dTdy2,dTdy3,dTdy4
  real(dp)::dTdz1,dTdz2,dTdz3,dTdz4
  real(dp)::bx1,bx2,bx3,bx4
  real(dp)::by1,by2,by3,by4
  real(dp)::bz1,bz2,bz3,bz4
  real(dp)::fy1,fy2,fy3,fy4
  real(dp)::kpary1,kpary2,kpary3,kpary4,oneminuskperp,dmean,tmean,maxflux

  ! Local scalar variables
  integer::i,j,k,l,ivar
  integer::ilo,ihi,jlo,jhi,klo,khi

  oneovertwodx =0.50d0/dx
  oneoverfourdx=0.25d0/dx
  oneminuskperp=1.0d0-k_perp

  ilo=MIN(1,iu1+2); ihi=MAX(1,iu2-2)
  klo=MIN(1,ku1+2); khi=MAX(1,ku2-2)

if(isotrope_cond)then
  do k=klo,khi
  do j=jf1,jf2
  do i=ilo,ihi
     do l = 1, ngrid
        dTdy1  =(Temp(l,i,j,k)-Temp(l,i,j-1,k))/dx
        kpary1=0.5d0*(kspitzer(l,i,j-1,k)+kspitzer(l,i,j,k))
        fy    =kpary1*dTdy1
        famr=max(ffdx(l,i,j,k),ffdx(l,i,j-1,k))
        fy=fy/famr
        myflux(l,i,j,k)=fy*dt/dx
        if(compute==3)myflux(l,i,j,k)=kpary1*dt/dx**2
     enddo
  enddo
  enddo
  enddo

else

  if (.not.slopelim_cond)then
  do k=klo,khi
  do j=jf1,jf2
  do i=ilo,ihi
     do l = 1, ngrid
#if NDIM==2
!!$        --------
!!$        |      |
!!$        |      |
!!$        |      |
!!$        1------2
        
        if(compute .ne. 3)then
           dTdx1=(Temp(l,i  ,j  ,k)+Temp(l,i  ,j-1,k) &
                - Temp(l,i-1,j  ,k)-Temp(l,i-1,j-1,k))*oneovertwodx
           dTdx2=(Temp(l,i+1,j  ,k)+Temp(l,i+1,j-1,k) &
             - Temp(l,i  ,j  ,k)-Temp(l,i  ,j-1,k))*oneovertwodx        
           
           dTdy1=(Temp(l,i  ,j  ,k)+Temp(l,i-1,j  ,k) &
                - Temp(l,i  ,j-1,k)-Temp(l,i-1,j-1,k))*oneovertwodx
           dTdy2=(Temp(l,i  ,j  ,k)+Temp(l,i+1,j  ,k) &
                - Temp(l,i  ,j-1,k)-Temp(l,i+1,j-1,k))*oneovertwodx
        end if

        bx1=0.5d0*(bf(l,i  ,j-1,k,1)+bf(l,i  ,j  ,k,1))
        bx2=0.5d0*(bf(l,i+1,j  ,k,1)+bf(l,i+1,j-1,k,1))
        
        by1=0.5d0*(bf(l,i-1,j  ,k,2)+bf(l,i  ,j  ,k,2))
        by2=0.5d0*(bf(l,i  ,j  ,k,2)+bf(l,i+1,j  ,k,2))
        
        Bnorm=sqrt(bx1*bx1+by1*by1)
        if(Bnorm.gt.0.0)then
           bx1=bx1/Bnorm
           by1=by1/Bnorm
        endif
        Bnorm=sqrt(bx2*bx2+by2*by2)
        if(Bnorm.gt.0.0)then
           bx2=bx2/Bnorm
           by2=by2/Bnorm
        endif

        kpary1=0.25d0*(kspitzer(l,i  ,j  ,k)+kspitzer(l,i  ,j-1,k) &
             +      kspitzer(l,i-1,j  ,k)+kspitzer(l,i-1,j-1,k))
        kpary2=0.25d0*(kspitzer(l,i  ,j  ,k)+kspitzer(l,i+1,j  ,k) &
             +      kspitzer(l,i  ,j-1,k)+kspitzer(l,i+1,j-1,k))

        if(compute .ne. 3)then   
           fy1=kpary1*(by1*oneminuskperp*(bx1*dTdx1+by1*dTdy1)+k_perp*dTdy1)
           fy2=kpary2*(by2*oneminuskperp*(bx2*dTdx2+by2*dTdy2)+k_perp*dTdy2)
        else ! Preconditionner
           fy1=kpary1*(by1*oneminuskperp*(bx1+by1)+k_perp)
           fy2=kpary2*(by2*oneminuskperp*(bx2+by2)+k_perp)
        end if
        fy=0.5d0*(fy1+fy2)
#endif
#if NDIM==3
!!$          ---------
!!$         / |      /|
!!$        /  |     / |
!!$        3------4   |
!!$        |  |   |   |
!!$        | /    |  /
!!$        |/     | /
!!$        1------2
        
        ! Centered symmetric scheme
        if(compute .ne. 3)then
           dTdx1=(Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  )+Temp(l,i  ,j  ,k-1)+Temp(l,i  ,j-1,k-1) &
                - Temp(l,i-1,j  ,k  )-Temp(l,i-1,j-1,k  )-Temp(l,i-1,j  ,k-1)-Temp(l,i-1,j-1,k-1))*oneoverfourdx
           dTdx2=(Temp(l,i+1,j  ,k  )+Temp(l,i+1,j-1,k  )+Temp(l,i+1,j  ,k-1)+Temp(l,i+1,j-1,k-1) &
                - Temp(l,i  ,j  ,k  )-Temp(l,i  ,j-1,k  )-Temp(l,i  ,j  ,k-1)-Temp(l,i  ,j-1,k-1))*oneoverfourdx
           dTdx3=(Temp(l,i  ,j  ,k+1)+Temp(l,i  ,j-1,k+1)+Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  ) &
                - Temp(l,i-1,j  ,k+1)-Temp(l,i-1,j-1,k+1)-Temp(l,i-1,j  ,k  )-Temp(l,i-1,j-1,k  ))*oneoverfourdx
           dTdx4=(Temp(l,i+1,j  ,k+1)+Temp(l,i+1,j-1,k+1)+Temp(l,i+1,j  ,k  )+Temp(l,i+1,j-1,k  ) &
                - Temp(l,i  ,j  ,k+1)-Temp(l,i  ,j-1,k+1)-Temp(l,i  ,j  ,k  )-Temp(l,i  ,j-1,k  ))*oneoverfourdx
           
           dTdy1=(Temp(l,i  ,j  ,k  )+Temp(l,i-1,j  ,k  )+Temp(l,i  ,j  ,k-1)+Temp(l,i-1,j  ,k-1) &
                - Temp(l,i  ,j-1,k  )-Temp(l,i-1,j-1,k  )-Temp(l,i  ,j-1,k-1)-Temp(l,i-1,j-1,k-1))*oneoverfourdx
           dTdy2=(Temp(l,i+1,j  ,k  )+Temp(l,i  ,j  ,k  )+Temp(l,i+1,j  ,k-1)+Temp(l,i  ,j  ,k-1) &
                - Temp(l,i+1,j-1,k  )-Temp(l,i  ,j-1,k  )-Temp(l,i+1,j-1,k-1)-Temp(l,i  ,j-1,k-1))*oneoverfourdx
           dTdy3=(Temp(l,i  ,j  ,k+1)+Temp(l,i-1,j  ,k+1)+Temp(l,i  ,j  ,k  )+Temp(l,i-1,j  ,k  ) &
                - Temp(l,i  ,j-1,k+1)-Temp(l,i-1,j-1,k+1)-Temp(l,i  ,j-1,k  )-Temp(l,i-1,j-1,k  ))*oneoverfourdx
           dTdy4=(Temp(l,i+1,j  ,k+1)+Temp(l,i  ,j  ,k+1)+Temp(l,i+1,j  ,k  )+Temp(l,i  ,j  ,k  ) &
                - Temp(l,i+1,j-1,k+1)-Temp(l,i  ,j-1,k+1)-Temp(l,i+1,j-1,k  )-Temp(l,i  ,j-1,k  ))*oneoverfourdx
           
           dTdz1=(Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  )+Temp(l,i-1,j  ,k  )+Temp(l,i-1,j-1,k  ) &
                - Temp(l,i  ,j  ,k-1)-Temp(l,i  ,j-1,k-1)-Temp(l,i-1,j  ,k-1)-Temp(l,i-1,j-1,k-1))*oneoverfourdx
           dTdz2=(Temp(l,i+1,j  ,k  )+Temp(l,i+1,j-1,k  )+Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  ) &
                - Temp(l,i+1,j  ,k-1)-Temp(l,i+1,j-1,k-1)-Temp(l,i  ,j  ,k-1)-Temp(l,i  ,j-1,k-1))*oneoverfourdx
           dTdz3=(Temp(l,i  ,j  ,k+1)+Temp(l,i-1,j  ,k+1)+Temp(l,i  ,j-1,k+1)+Temp(l,i-1,j-1,k+1) &
                - Temp(l,i  ,j  ,k  )-Temp(l,i-1,j  ,k  )-Temp(l,i  ,j-1,k  )-Temp(l,i-1,j-1,k  ))*oneoverfourdx
           dTdz4=(Temp(l,i+1,j  ,k+1)+Temp(l,i  ,j  ,k+1)+Temp(l,i+1,j-1,k+1)+Temp(l,i  ,j-1,k+1) &
                - Temp(l,i+1,j  ,k  )-Temp(l,i  ,j  ,k  )-Temp(l,i+1,j-1,k  )-Temp(l,i  ,j-1,k  ))*oneoverfourdx
        end if

        bx1=0.25d0*(bf(l,i  ,j-1,k-1,1)+bf(l,i  ,j  ,k-1,1)+bf(l,i  ,j-1,k  ,1)+bf(l,i  ,j  ,k  ,1))
        bx2=0.25d0*(bf(l,i+1,j-1,k-1,1)+bf(l,i+1,j  ,k-1,1)+bf(l,i+1,j-1,k  ,1)+bf(l,i+1,j  ,k  ,1))
        bx3=0.25d0*(bf(l,i  ,j-1,k  ,1)+bf(l,i  ,j  ,k  ,1)+bf(l,i  ,j-1,k+1,1)+bf(l,i  ,j  ,k+1,1))
        bx4=0.25d0*(bf(l,i+1,j-1,k  ,1)+bf(l,i+1,j  ,k  ,1)+bf(l,i+1,j-1,k+1,1)+bf(l,i+1,j  ,k+1,1))

        by1=0.25d0*(bf(l,i-1,j  ,k-1,2)+bf(l,i  ,j  ,k-1,2)+bf(l,i-1,j  ,k  ,2)+bf(l,i  ,j  ,k  ,2))
        by2=0.25d0*(bf(l,i  ,j  ,k-1,2)+bf(l,i+1,j  ,k-1,2)+bf(l,i  ,j  ,k  ,2)+bf(l,i+1,j  ,k  ,2))
        by3=0.25d0*(bf(l,i-1,j  ,k  ,2)+bf(l,i  ,j  ,k  ,2)+bf(l,i-1,j  ,k+1,2)+bf(l,i  ,j  ,k+1,2))
        by4=0.25d0*(bf(l,i  ,j  ,k  ,2)+bf(l,i+1,j  ,k  ,2)+bf(l,i  ,j  ,k+1,2)+bf(l,i+1,j  ,k+1,2))
        
        bz1=0.25d0*(bf(l,i-1,j-1,k  ,3)+bf(l,i-1,j  ,k  ,3)+bf(l,i  ,j-1,k  ,3)+bf(l,i  ,j  ,k  ,3))
        bz2=0.25d0*(bf(l,i  ,j-1,k  ,3)+bf(l,i  ,j  ,k  ,3)+bf(l,i+1,j-1,k  ,3)+bf(l,i+1,j  ,k  ,3))
        bz3=0.25d0*(bf(l,i  ,j-1,k+1,3)+bf(l,i  ,j  ,k+1,3)+bf(l,i-1,j-1,k+1,3)+bf(l,i-1,j  ,k+1,3))
        bz4=0.25d0*(bf(l,i+1,j-1,k+1,3)+bf(l,i+1,j  ,k+1,3)+bf(l,i  ,j-1,k+1,3)+bf(l,i  ,j  ,k+1,3))

        Bnorm=sqrt(bx1*bx1+by1*by1+bz1*bz1)
        if(Bnorm.gt.0.0)then
           bx1=bx1/Bnorm
           by1=by1/Bnorm
           bz1=bz1/Bnorm
        endif
        Bnorm=sqrt(bx2*bx2+by2*by2+bz2*bz2)
        if(Bnorm.gt.0.0)then
           bx2=bx2/Bnorm
           by2=by2/Bnorm
           bz2=bz2/Bnorm
        endif
        Bnorm=sqrt(bx3*bx3+by3*by3+bz3*bz3)
        if(Bnorm.gt.0.0)then
           bx3=bx3/Bnorm
           by3=by3/Bnorm
           bz3=bz3/Bnorm
        endif
        Bnorm=sqrt(bx4*bx4+by4*by4+bz4*bz4)
        if(Bnorm.gt.0.0)then
           bx4=bx4/Bnorm
           by4=by4/Bnorm
           bz4=bz4/Bnorm
        endif

        kpary1=0.125d0*(kspitzer(l,i  ,j  ,k  )+kspitzer(l,i  ,j-1,k  )+kspitzer(l,i  ,j  ,k-1) &
             +      kspitzer(l,i  ,j-1,k-1)+kspitzer(l,i-1,j  ,k  )+kspitzer(l,i-1,j-1,k  ) &
             +      kspitzer(l,i-1,j  ,k-1)+kspitzer(l,i-1,j-1,k-1))
        kpary2=0.125d0*(kspitzer(l,i+1,j  ,k  )+kspitzer(l,i+1,j-1,k  )+kspitzer(l,i+1,j  ,k-1) &
             +      kspitzer(l,i+1,j-1,k-1)+kspitzer(l,i  ,j  ,k  )+kspitzer(l,i  ,j-1,k  ) &
             +      kspitzer(l,i  ,j  ,k-1)+kspitzer(l,i  ,j-1,k-1))
        kpary3=0.125d0*(kspitzer(l,i  ,j  ,k+1)+kspitzer(l,i  ,j-1,k+1)+kspitzer(l,i  ,j  ,k  ) &
             +      kspitzer(l,i  ,j-1,k  )+kspitzer(l,i-1,j  ,k+1)+kspitzer(l,i-1,j-1,k+1) &
             +      kspitzer(l,i-1,j  ,k  )+kspitzer(l,i-1,j-1,k  ))
        kpary4=0.125d0*(kspitzer(l,i+1,j  ,k+1)+kspitzer(l,i+1,j-1,k+1)+kspitzer(l,i+1,j  ,k  ) &
             +      kspitzer(l,i+1,j-1,k  )+kspitzer(l,i  ,j  ,k+1)+kspitzer(l,i  ,j-1,k+1) &
             +      kspitzer(l,i  ,j  ,k  )+kspitzer(l,i  ,j-1,k  ))

        if(compute .ne. 3)then        
           fy1=kpary1*(by1*oneminuskperp*(bx1*dTdx1+by1*dTdy1+bz1*dTdz1)+k_perp*dTdy1)
           fy2=kpary2*(by2*oneminuskperp*(bx2*dTdx2+by2*dTdy2+bz2*dTdz2)+k_perp*dTdy2)
           fy3=kpary3*(by3*oneminuskperp*(bx3*dTdx3+by3*dTdy3+bz3*dTdz3)+k_perp*dTdy3)
           fy4=kpary4*(by4*oneminuskperp*(bx4*dTdx4+by4*dTdy4+bz4*dTdz4)+k_perp*dTdy4)
        else
           fy1=kpary1*(by1*oneminuskperp*(bx1+by1+bz1)+k_perp)
           fy2=kpary2*(by2*oneminuskperp*(bx2+by2+bz2)+k_perp)
           fy3=kpary3*(by3*oneminuskperp*(bx3+by3+bz3)+k_perp)
           fy4=kpary4*(by4*oneminuskperp*(bx4+by4+bz4)+k_perp)
        end if
        fy=0.25d0*(fy1+fy2+fy3+fy4)
#endif
        if(saturation)then
           dmean = 0.5d0*(dens (l,i,j-1,k)+dens (l,i,j,k))
           tmean = 0.5d0*(Temp2(l,i,j-1,k)+Temp2(l,i,j,k))
           if(compute .ne. 3)then
              maxflux=fudge_sat*tmean**1.5*dmean
           else
              maxflux=fudge_sat*tmean**1.5*dmean/(Temp2(l,i,j,k)-Temp2(l,i,j-1,k))*dx*famr
           endif
           if(fy.lt.0.0d0)fy=MAX(fy,-maxflux)
           if(fy.gt.0.0d0)fy=MIN(fy, maxflux)
        endif

        myflux(l,i,j,k)=fy*dt/dx
     enddo
  enddo
  enddo
  enddo
  endif

  if (slopelim_cond)then
     ! TODO
  endif
endif

end subroutine cmpYheatflx
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine cmpZheatflx(Temp,dens,bf,kspitzer,myflux,dx,dy,dz,dt,ngrid,compute,ffdx,Temp2)
  use amr_parameters
  use const             
  use hydro_parameters
  implicit none 

  integer ::ngrid,compute
  real(dp)::dx,dy,dz,dt

  ! Output fluxes
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2)::myflux

  ! Primitive variables
  real(dp),dimension(1:nvector,iu1:iu2+1,ju1:ju2+1,ku1:ku2+1,1:3)::bf  
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::kspitzer
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::Temp,Temp2,dens
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::ffdx

  real(dp)::Bnorm,fz,oneovertwodx,oneoverfourdx,famr
  real(dp)::dTdx1,dTdx2,dTdx3,dTdx4
  real(dp)::dTdy1,dTdy2,dTdy3,dTdy4
  real(dp)::dTdz1,dTdz2,dTdz3,dTdz4
  real(dp)::bx1,bx2,bx3,bx4
  real(dp)::by1,by2,by3,by4
  real(dp)::bz1,bz2,bz3,bz4
  real(dp)::fz1,fz2,fz3,fz4
  real(dp)::kparz1,kparz2,kparz3,kparz4,oneminuskperp,dmean,tmean,maxflux

  ! Local scalar variables
  integer::i,j,k,l,ivar
  integer::ilo,ihi,jlo,jhi,klo,khi

  oneovertwodx =0.50d0/dx
  oneoverfourdx=0.25d0/dx
  oneminuskperp=1.0d0-k_perp

  ilo=MIN(1,iu1+2); ihi=MAX(1,iu2-2)
  jlo=MIN(1,ju1+2); jhi=MAX(1,ju2-2)

if(isotrope_cond)then
  do k=kf1,kf2
  do j=jlo,jhi
  do i=ilo,ihi
     do l = 1, ngrid
        dTdz1  =(Temp(l,i,j,k)-Temp(l,i,j,k-1))/dx
        kparz1=0.5d0*(kspitzer(l,i,j,k-1)+kspitzer(l,i,j,k))
        fz    =kparz1*dTdz1
        famr=max(ffdx(l,i,j,k),ffdx(l,i,j,k-1))
        fz=fz/famr
        myflux(l,i,j,k)=fz*dt/dx
        if(compute==3)myflux(l,i,j,k)=kparz1*dt/dx**2
     enddo
  enddo
  enddo
  enddo

else

  if (.not.slopelim_cond)then
  do k=kf1,kf2
  do j=jlo,jhi
  do i=ilo,ihi
     do l = 1, ngrid
#if NDIM==3
!!$          ---------
!!$         / |      /|
!!$        /  |     / |
!!$        --------   |
!!$        |  |   |   |
!!$        | /3   |  /4
!!$        |/     | /
!!$        1------2
        
        ! Centered symmetric scheme
        if(compute .ne. 3)then        
           dTdx1=(Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  )+Temp(l,i  ,j  ,k-1)+Temp(l,i  ,j-1,k-1) &
                - Temp(l,i-1,j  ,k  )-Temp(l,i-1,j-1,k  )-Temp(l,i-1,j  ,k-1)-Temp(l,i-1,j-1,k-1))*oneoverfourdx
           dTdx2=(Temp(l,i+1,j  ,k  )+Temp(l,i+1,j-1,k  )+Temp(l,i+1,j  ,k-1)+Temp(l,i+1,j-1,k-1) &
                - Temp(l,i  ,j  ,k  )-Temp(l,i  ,j-1,k  )-Temp(l,i  ,j  ,k-1)-Temp(l,i  ,j-1,k-1))*oneoverfourdx
           dTdx3=(Temp(l,i  ,j+1,k  )+Temp(l,i  ,j  ,k  )+Temp(l,i  ,j+1,k-1)+Temp(l,i  ,j  ,k-1) &
                - Temp(l,i-1,j+1,k  )-Temp(l,i-1,j  ,k  )-Temp(l,i-1,j+1,k-1)-Temp(l,i-1,j  ,k-1))*oneoverfourdx
           dTdx4=(Temp(l,i+1,j+1,k  )+Temp(l,i+1,j  ,k  )+Temp(l,i+1,j+1,k-1)+Temp(l,i+1,j  ,k-1) &
                - Temp(l,i  ,j+1,k  )-Temp(l,i  ,j  ,k  )-Temp(l,i  ,j+1,k-1)-Temp(l,i  ,j  ,k-1))*oneoverfourdx
           
           dTdy1=(Temp(l,i  ,j  ,k  )+Temp(l,i-1,j  ,k  )+Temp(l,i  ,j  ,k-1)+Temp(l,i-1,j  ,k-1) &
                - Temp(l,i  ,j-1,k  )-Temp(l,i-1,j-1,k  )-Temp(l,i  ,j-1,k-1)-Temp(l,i-1,j-1,k-1))*oneoverfourdx
           dTdy2=(Temp(l,i+1,j  ,k  )+Temp(l,i  ,j  ,k  )+Temp(l,i+1,j  ,k-1)+Temp(l,i  ,j  ,k-1) &
                - Temp(l,i+1,j-1,k  )-Temp(l,i  ,j-1,k  )-Temp(l,i+1,j-1,k-1)-Temp(l,i  ,j-1,k-1))*oneoverfourdx
           dTdy3=(Temp(l,i  ,j+1,k  )+Temp(l,i-1,j+1,k  )+Temp(l,i  ,j+1,k-1)+Temp(l,i-1,j+1,k-1) &
                - Temp(l,i  ,j  ,k  )-Temp(l,i-1,j  ,k  )-Temp(l,i  ,j  ,k-1)-Temp(l,i-1,j  ,k-1))*oneoverfourdx
           dTdy4=(Temp(l,i+1,j+1,k  )+Temp(l,i  ,j+1,k  )+Temp(l,i+1,j+1,k-1)+Temp(l,i  ,j+1,k-1) &
                - Temp(l,i+1,j  ,k  )-Temp(l,i  ,j  ,k  )-Temp(l,i+1,j  ,k-1)-Temp(l,i  ,j  ,k-1))*oneoverfourdx
           
           dTdz1=(Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  )+Temp(l,i-1,j  ,k  )+Temp(l,i-1,j-1,k  ) &
                - Temp(l,i  ,j  ,k-1)-Temp(l,i  ,j-1,k-1)-Temp(l,i-1,j  ,k-1)-Temp(l,i-1,j-1,k-1))*oneoverfourdx
           dTdz2=(Temp(l,i+1,j  ,k  )+Temp(l,i+1,j-1,k  )+Temp(l,i  ,j  ,k  )+Temp(l,i  ,j-1,k  ) &
                - Temp(l,i+1,j  ,k-1)-Temp(l,i+1,j-1,k-1)-Temp(l,i  ,j  ,k-1)-Temp(l,i  ,j-1,k-1))*oneoverfourdx
           dTdz3=(Temp(l,i  ,j+1,k  )+Temp(l,i  ,j  ,k  )+Temp(l,i-1,j+1,k  )+Temp(l,i-1,j  ,k  ) &
                - Temp(l,i  ,j+1,k-1)-Temp(l,i  ,j  ,k-1)-Temp(l,i-1,j+1,k-1)-Temp(l,i-1,j  ,k-1))*oneoverfourdx
           dTdz4=(Temp(l,i+1,j+1,k  )+Temp(l,i+1,j  ,k  )+Temp(l,i  ,j+1,k  )+Temp(l,i  ,j  ,k  ) &
                - Temp(l,i+1,j+1,k-1)-Temp(l,i+1,j  ,k-1)-Temp(l,i  ,j+1,k-1)-Temp(l,i  ,j  ,k-1))*oneoverfourdx
        end if
        
        bx1=0.25d0*(bf(l,i  ,j-1,k-1,1)+bf(l,i  ,j  ,k-1,1)+bf(l,i  ,j-1,k  ,1)+bf(l,i  ,j  ,k  ,1))
        bx2=0.25d0*(bf(l,i+1,j-1,k-1,1)+bf(l,i+1,j  ,k-1,1)+bf(l,i+1,j-1,k  ,1)+bf(l,i+1,j  ,k  ,1))
        bx3=0.25d0*(bf(l,i  ,j  ,k-1,1)+bf(l,i  ,j+1,k-1,1)+bf(l,i  ,j  ,k  ,1)+bf(l,i  ,j+1,k  ,1))
        bx4=0.25d0*(bf(l,i+1,j  ,k-1,1)+bf(l,i+1,j+1,k-1,1)+bf(l,i+1,j  ,k  ,1)+bf(l,i+1,j+1,k  ,1))

        by1=0.25d0*(bf(l,i-1,j  ,k-1,2)+bf(l,i  ,j  ,k-1,2)+bf(l,i-1,j  ,k  ,2)+bf(l,i  ,j  ,k  ,2))
        by2=0.25d0*(bf(l,i  ,j  ,k-1,2)+bf(l,i+1,j  ,k-1,2)+bf(l,i  ,j  ,k  ,2)+bf(l,i+1,j  ,k  ,2))
        by3=0.25d0*(bf(l,i-1,j+1,k-1,2)+bf(l,i  ,j+1,k-1,2)+bf(l,i-1,j+1,k  ,2)+bf(l,i  ,j+1,k  ,2))
        by4=0.25d0*(bf(l,i  ,j+1,k-1,2)+bf(l,i+1,j+1,k-1,2)+bf(l,i  ,j+1,k  ,2)+bf(l,i+1,j+1,k  ,2))
        
        bz1=0.25d0*(bf(l,i-1,j-1,k  ,3)+bf(l,i-1,j  ,k  ,3)+bf(l,i  ,j-1,k  ,3)+bf(l,i  ,j  ,k  ,3))
        bz2=0.25d0*(bf(l,i  ,j-1,k  ,3)+bf(l,i  ,j  ,k  ,3)+bf(l,i+1,j-1,k  ,3)+bf(l,i+1,j  ,k  ,3))
        bz3=0.25d0*(bf(l,i-1,j  ,k  ,3)+bf(l,i-1,j+1,k  ,3)+bf(l,i  ,j  ,k  ,3)+bf(l,i  ,j+1,k  ,3))
        bz4=0.25d0*(bf(l,i  ,j  ,k  ,3)+bf(l,i  ,j+1,k  ,3)+bf(l,i+1,j  ,k  ,3)+bf(l,i+1,j+1,k  ,3))

        Bnorm=sqrt(bx1*bx1+by1*by1+bz1*bz1)
        if(Bnorm.gt.0.0)then
           bx1=bx1/Bnorm
           by1=by1/Bnorm
           bz1=bz1/Bnorm
        endif
        Bnorm=sqrt(bx2*bx2+by2*by2+bz2*bz2)
        if(Bnorm.gt.0.0)then
           bx2=bx2/Bnorm
           by2=by2/Bnorm
           bz2=bz2/Bnorm
        endif
        Bnorm=sqrt(bx3*bx3+by3*by3+bz3*bz3)
        if(Bnorm.gt.0.0)then
           bx3=bx3/Bnorm
           by3=by3/Bnorm
           bz3=bz3/Bnorm
        endif
        Bnorm=sqrt(bx4*bx4+by4*by4+bz4*bz4)
        if(Bnorm.gt.0.0)then
           bx4=bx4/Bnorm
           by4=by4/Bnorm
           bz4=bz4/Bnorm
        endif

        kparz1=0.125d0*(kspitzer(l,i  ,j  ,k  )+kspitzer(l,i  ,j-1,k  )+kspitzer(l,i  ,j  ,k-1) &
             +      kspitzer(l,i  ,j-1,k-1)+kspitzer(l,i-1,j  ,k  )+kspitzer(l,i-1,j-1,k  ) &
             +      kspitzer(l,i-1,j  ,k-1)+kspitzer(l,i-1,j-1,k-1))
        kparz2=0.125d0*(kspitzer(l,i+1,j  ,k  )+kspitzer(l,i+1,j-1,k  )+kspitzer(l,i+1,j  ,k-1) &
             +      kspitzer(l,i+1,j-1,k-1)+kspitzer(l,i  ,j  ,k  )+kspitzer(l,i  ,j-1,k  ) &
             +      kspitzer(l,i  ,j  ,k-1)+kspitzer(l,i  ,j-1,k-1))
        kparz3=0.125d0*(kspitzer(l,i  ,j+1,k  )+kspitzer(l,i  ,j  ,k  )+kspitzer(l,i  ,j+1,k-1) &
             +      kspitzer(l,i  ,j  ,k-1)+kspitzer(l,i-1,j+1,k  )+kspitzer(l,i-1,j  ,k  ) &
             +      kspitzer(l,i-1,j+1,k-1)+kspitzer(l,i-1,j  ,k-1))
        kparz4=0.125d0*(kspitzer(l,i+1,j+1,k  )+kspitzer(l,i+1,j  ,k  )+kspitzer(l,i+1,j+1,k-1) &
             +      kspitzer(l,i+1,j  ,k-1)+kspitzer(l,i  ,j+1,k  )+kspitzer(l,i  ,j  ,k  ) &
             +      kspitzer(l,i  ,j+1,k-1)+kspitzer(l,i  ,j  ,k-1))

        if(compute .ne. 3)then        
           fz1=kparz1*(bz1*oneminuskperp*(bx1*dTdx1+by1*dTdy1+bz1*dTdz1)+k_perp*dTdz1)
           fz2=kparz2*(bz2*oneminuskperp*(bx2*dTdx2+by2*dTdy2+bz2*dTdz2)+k_perp*dTdz2)
           fz3=kparz3*(bz3*oneminuskperp*(bx3*dTdx3+by3*dTdy3+bz3*dTdz3)+k_perp*dTdz3)
           fz4=kparz4*(bz4*oneminuskperp*(bx4*dTdx4+by4*dTdy4+bz4*dTdz4)+k_perp*dTdz4)
        else
           fz1=kparz1*(bz1*oneminuskperp*(bx1+by1+bz1)+k_perp)
           fz2=kparz2*(bz2*oneminuskperp*(bx2+by2+bz2)+k_perp)
           fz3=kparz3*(bz3*oneminuskperp*(bx3+by3+bz3)+k_perp)
           fz4=kparz4*(bz4*oneminuskperp*(bx4+by4+bz4)+k_perp)
        end if
        fz=0.25d0*(fz1+fz2+fz3+fz4)
#endif
        if(saturation)then
           dmean = 0.5d0*(dens (l,i,j,k-1)+dens (l,i,j,k))
           tmean = 0.5d0*(Temp2(l,i,j,k-1)+Temp2(l,i,j,k))
           if(compute .ne. 3)then
              maxflux=fudge_sat*tmean**1.5*dmean
           else
              maxflux=fudge_sat*tmean**1.5*dmean/(Temp2(l,i,j,k)-Temp2(l,i,j,k-1))*dx*famr
           endif
           if(fz.lt.0.0d0)fz=MAX(fz,-maxflux)
           if(fz.gt.0.0d0)fz=MIN(fz, maxflux)
        endif

        myflux(l,i,j,k)=fz*dt/dx
     enddo
  enddo
  enddo
  enddo
  endif

  if (slopelim_cond)then
     ! TODO
  endif
endif

end subroutine cmpZheatflx
!###########################################################
!###########################################################
!###########################################################
!###########################################################
